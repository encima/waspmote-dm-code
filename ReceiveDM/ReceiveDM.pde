//MAC Address: 0013A20040763B80
//setup vars
uint8_t  PANID[2]={0x12,0x34};
char*  KEY="WaspmoteKey";
int count = 0;
char data[200];
char* macHigh="          ";
char* macLow="           ";

//loop vars
long previous;

void setup()
{
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);

  // Powers XBee
  xbeeDM.ON();

  //get mac address
  int counter = 0;  
  while(xbeeDM.getOwnMac()==1&&counter<4){
    xbeeDM.getOwnMac();
    counter++;
  }

  Utils.hex2str(xbeeDM.sourceMacHigh,macHigh,4);
  Utils.hex2str(xbeeDM.sourceMacLow,macLow,4);
  sprintf(data,"-mac:%s%s",macHigh,macLow,'%','\r','\n');
  
  xbeeDM.setNodeIdentifier("dgfc-01");
  
  XBee.println(data);
}

void loop() {
  previous=millis();
  XBee.println("Starting Loop!");
  while( (millis()-previous) < 20000 )
  {
    if( XBee.available() )
    {
      XBee.println("Treading available data");
      xbeeDM.treatData(); 
      if(xbeeDM.pos>0)
      {	
        XBee.println("Received");
        XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[0]);
        XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[3]);
      }else{
        XBee.println("Nothing Received");
      }
    }
  }
  delay(1000);
}


