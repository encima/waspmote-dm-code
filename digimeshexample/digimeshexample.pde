uint8_t  PANID[8]={0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08}; 
char*  NETKEY="WaspmoteKey"; 
char*  LINKKEY="WaspmoteKey"; 
 
void setup() 
{ 
  // Inits the XBee ZigBee library 
  xbeeZB.init(ZIGBEE,FREQ2_4G,NORMAL); 
 
  // Powers XBee 
  xbeeZB.ON(); 
  
    // Getting Channel 
  xbee802.getChannel(); 
  if( !xbee802.error_AT ){ 
    XBee.print("Channel is: "); 
    XBee.println(xbeeZB.channel,HEX); 
  } 
  else XBee.println("Error getting channel"); 
 
  // Chosing a PANID : PANID=0x0102030405060708 
  xbeeZB.setPAN(PANID); 
  if( !xbeeZB.error_AT ) XBee.println("PANID set OK"); 
  else XBee.println("Error while changing PANID"); 
 
  // Enabling security 
  xbeeZB.encryptionMode(1); 
  if( !xbeeZB.error_AT ) XBee.println("Security enabled"); 
  else XBee.println("Error while enabling security"); 
 
  // Configuring Trust Center 
  !xbeeZB.setEncryptionOptions(0x02); 
  if( !xbeeZB.error_AT ) XBee.println("Security options configured"); 
  else XBee.println("Error while configuring security"); 
 
  // Setting Link Key 
  xbeeZB.setLinkKey(LINKKEY); 
  if( !xbeeZB.error_AT ) XBee.println("Link Key set OK"); 
  else XBee.println("Error while setting Key"); 
 
  // Setting Network Key (only in Coordinator) 
  xbeeZB.setNetworkKey(NETKEY); 
  if( !xbeeZB.error_AT ) XBee.println("Network Key set OK"); 
  else XBee.println("Error while setting Key"); 
 
  xbeeZB.setAPSencryption(XBEE_ON); 
  if( !xbeeZB.error_AT ) XBee.println("APS Encryption set OK"); 
  else XBee.println("Error while setting APS Encryption"); 
 
  // Keep values 
  xbeeZB.writeValues(); 
  if( !xbeeZB.error_AT ) XBee.println("Changes stored OK"); 
  else XBee.println("Error while storing values"); 
} 
 
void loop() 
{ 
  XBee.println("Network Set Up, running loop method..."); 
  delay(3000); 
} 
