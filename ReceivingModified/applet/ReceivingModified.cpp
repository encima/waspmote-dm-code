/*
 *  ------Waspmote XBee DigiMesh Sending & Receiving Example------
 *
 *  Explanation: This example shows how to send and receive packets
 *  using Waspmote XBee DigiMesh API
 *
 *  This code sends a packet to another node and waits for an answer from
 *  it. When the answer is received it is shown.
 *
 *  Copyright (C) 2009 Libelium Comunicaciones Distribuidas S.L.
 *  http://www.libelium.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Version:                0.2
 *  Design:                 David Gasc\u00f3n
 *  Implementation:    Alberto Bielsa
 */
 
 #include "WProgram.h"
void setup();
void loop();
packetXBee* paq_sent;
 int8_t state=0;
 long previous=0;
 char*  data="Test message!";
 
void setup()
{
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  
  // Powers XBee
  xbeeDM.ON();
}

void loop()
{
  // Set params to send
  paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
  paq_sent->mode=UNICAST;
  paq_sent->MY_known=0;
  paq_sent->packetID=0x52;
  paq_sent->opt=0; 
  xbeeDM.hops=0;
  xbeeDM.setOriginParams(paq_sent, "5678", MY_TYPE);
  xbeeDM.setDestinationParams(paq_sent, "0013A200407B", data, MAC_TYPE, DATA_ABSOLUTE);
  xbeeDM.sendXBee(paq_sent);
  if( !xbeeDM.error_TX )
  {
    XBee.println("ok");
  }
  free(paq_sent);
  paq_sent=NULL;
  
  // Waiting the answer
  previous=millis();
  while( (millis()-previous) < 20000 )
  {
    if( XBee.available() )
    {
      xbeeDM.treatData();
      if( !xbeeDM.error_RX )
      {
        // Writing the parameters of the packet received
        while(xbeeDM.pos>0)
        {
          XBee.print("Network Address Source: ");
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Source: ");          
          for(int b=0;b<4;b++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[b],HEX);
          }
          for(int c=0;c<4;c++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSL[c],HEX);
          }
          XBee.println("");
          XBee.print("Network Address Origin: ");          
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Origin: ");          
          for(int d=0;d<4;d++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOH[d],HEX);
          }
          for(int e=0;e<4;e++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOL[e],HEX);
          }
          XBee.println("");
          XBee.print("RSSI: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->RSSI,HEX);
          XBee.println("");         
          XBee.print("16B(0) or 64B(1): ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->mode,HEX);
          XBee.println("");
          XBee.print("Data: ");                    
          for(int f=0;f<xbeeDM.packet_finished[xbeeDM.pos-1]->data_length;f++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->data[f],BYTE);
          }
          XBee.println("");
          XBee.print("PacketID: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->packetID,HEX);
          XBee.println("");      
          XBee.print("Type Source ID: ");                              
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->typeSourceID,HEX);
          XBee.println("");     
          XBee.print("Network Identifier Origin: ");          
          for(int g=0;g<4;g++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->niO[g],BYTE);
          }
          XBee.println("");  
          free(xbeeDM.packet_finished[xbeeDM.pos-1]);
          xbeeDM.packet_finished[xbeeDM.pos-1]=NULL;
          xbeeDM.pos--;
        }
        previous=millis();
      }
    }
  }
  delay(5000);
}



int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

