
//setup vars
#include "WProgram.h"
void setup();
void loop();
uint8_t  PANID[2]={0x12,0x34};
char*  KEY="WaspmoteKey";
char data[200];
char* macHigh="          ";
char* macLow="           ";

//loop vars
packetXBee* packet;

void setup()
{
    // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);

  // Powers XBee
  xbeeDM.ON();
  
  int counter = 0;  
  while(xbeeDM.getOwnMac()==1&&counter<4){
    xbeeDM.getOwnMac();
    counter++;
  }

  Utils.hex2str(xbeeDM.sourceMacHigh,macHigh,4);
  Utils.hex2str(xbeeDM.sourceMacLow,macLow,4);
  sprintf(data,"-mac:%s%s",macHigh,macLow,'%','\r','\n');
  
  xbeeDM.setNodeIdentifier("dgfc-02");
  
  Utils.setLED(LED0, LED_ON);
  Utils.setLED(LED0, LED_OFF);  
}

void loop()
{
 XBee.println("Prepping packet");
 packetXBee* paq_sent; // create packet to send
 paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
 paq_sent->mode=BROADCAST;			
 paq_sent->packetID=0x52;		
 paq_sent->opt=0x00;
 xbeeDM.setOriginParams(paq_sent, "5678", MY_TYPE); 
 xbeeDM.setDestinationParams(paq_sent, "000000000000FFFF", data, MAC_TYPE, DATA_ABSOLUTE);//NI Type = Node ID, MAC_TYPE = Mac Address Delivery
 xbeeDM.sendXBee(paq_sent); // Call function responsible to send data
 if( !xbeeDM.error_TX ) 
 { 
   XBee.println("Packet Sent!"); 
 }else{
   XBee.println(xbeeDM.error_TX);
 }
 free(paq_sent); 
 paq_sent=NULL; 
 delay(1000);
}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

