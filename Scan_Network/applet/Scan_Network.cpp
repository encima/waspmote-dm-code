#include "WProgram.h"
void setup();
void loop();
boolean scan();
void xbee_send_packages(char* dest, boolean mac);
void xbee_receive(int wait);
boolean setNetworkParams();
boolean createFileOnSD(char* filename);
boolean appendToSD(char* filename, char* data);
uint8_t  PANID[2]={0x33,0x31};
long previous=0;
char*  KEY="dgfc";
char* NODE = "DGFC-02";
packetXBee* paq_sent;
int count = 0;
int state = 0;
int delayPeriod = 1000;
boolean found = false;
boolean fileCreated = false;
char* file = "28022012-DGFC01.txt";

void setup() {
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  // Powers XBee
  xbeeDM.ON();
  boolean network = setNetworkParams();
  XBee.print("Network paramaters set: ");
  XBee.println(network);  
  
  //enable SD Card
  SD.ON();
  fileCreated = createFileOnSD(file);
  delay(10000);
}

void loop() {
    if(xbeeDM.totalScannedBrothers > 0) {
//      if((char)xbeeDM.getNodeIdentifier() == 'DGFC-01') {
        XBee.println('Running on temp head node, sending data');
        for(int i = 0; i < xbeeDM.totalScannedBrothers; i++) {
          xbee_send_packages(xbeeDM.scannedBrothers[i].NI, false);
        }
//      }else{
//        xbee_receive(20000);
//      }
    } else {
      scan();
    }
  delay(delayPeriod);
}

boolean scan()
{
  XBee.print(count);
  XBee.println("Starting Scan...");
  count++;
  xbeeDM.scanNetwork();
  XBee.print("Finished scan, No. of nodes found: ");
  XBee.println(xbeeDM.totalScannedBrothers); 
  if(xbeeDM.totalScannedBrothers > 0) {
//    SD.appendln(file, xbeeDM.scannedBrothers[0].NI)
    return true;
  }else{
    return false;
  }
}

void xbee_send_packages(char* dest, boolean mac) 
{ 
  char*  data="Test message!"; 

  // Set params to send 
  paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
  paq_sent->mode= BROADCAST;    // 0=unicast ; 1=broadcast ; 2=cluster ; 3=synchronization
  paq_sent->address_type= 1;  // 64 bit MAC
  paq_sent->MY_known= 0; 
  paq_sent->packetID= 0x52; 
  paq_sent->opt= 0x08;        // 0x08 - Multicast; 0x00 - OFF
  
  xbeeDM.setOriginParams(paq_sent, NODE, NI_TYPE);
  if(mac) 
  {
    xbeeDM.setDestinationParams(paq_sent, dest, data, NI_TYPE, DATA_ABSOLUTE); 
  }
  else
  {
    xbeeDM.setDestinationParams(paq_sent, dest, data, MAC_TYPE, DATA_ABSOLUTE);   
  }
  state= xbeeDM.sendXBee(paq_sent); 
  free(paq_sent); 
  paq_sent=NULL; 

  // Waiting the answer 
//  commented out for testing
//  xbee_receive(20000);

  XBee.println(": END");
   
}

void xbee_receive(int wait) {
  previous=millis();
  while( (millis()-previous) < wait )
  {
    if( XBee.available() )
    {
      xbeeDM.treatData();
      if( !xbeeDM.error_RX )
      {
        // Writing the parameters of the packet received
        while(xbeeDM.pos>0)
        {
          XBee.print("Network Address Source: ");
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Source: ");          
          for(int b=0;b<4;b++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[b],HEX);
          }
          for(int c=0;c<4;c++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSL[c],HEX);
          }
          XBee.println("");
          XBee.print("Network Address Origin: ");          
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Origin: ");          
          for(int d=0;d<4;d++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOH[d],HEX);
          }
          for(int e=0;e<4;e++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOL[e],HEX);
          }
          XBee.println("");
          XBee.print("RSSI: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->RSSI,HEX);
          XBee.println("");         
          XBee.print("16B(0) or 64B(1): ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->mode,HEX);
          XBee.println("");
          XBee.print("Data: ");                    
          for(int f=0;f<xbeeDM.packet_finished[xbeeDM.pos-1]->data_length;f++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->data[f],BYTE);
          }
          XBee.println("");
          XBee.print("PacketID: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->packetID,HEX);
          XBee.println("");      
          XBee.print("Type Source ID: ");                              
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->typeSourceID,HEX);
          XBee.println("");     
          XBee.print("Network Identifier Origin: ");          
          for(int g=0;g<4;g++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->niO[g],BYTE);
          }
          XBee.println("");  
          free(xbeeDM.packet_finished[xbeeDM.pos-1]);
          xbeeDM.packet_finished[xbeeDM.pos-1]=NULL;
          xbeeDM.pos--;
        }
        previous=millis();
      }
    }
  }
}

boolean setNetworkParams() {
  int success = true;

  // Chosing a channel : channel 0x0C-12
  xbeeDM.setChannel(0x17);
  if( !xbeeDM.error_AT ) {
    XBee.println("Channel set OK");
  }
  else
  {
    success = false;
    XBee.println("Error while changing channel");  
  }

  xbeeDM.setPAN(PANID);
  if( !xbeeDM.error_AT ) {
    XBee.println(xbeeDM.getPAN());
  }  else {
    success = false;
    XBee.println("Error while changing PANID");  
  }
//Disable encryption
//  xbeeDM.encryptionMode(0);
//  if( !xbeeDM.error_AT ) {
//    XBee.println("Security enabled");
//  } 
//  else {
//    success = 0;
//    XBee.println("Error while enabling security");  
//  }
//
//  xbeeDM.setLinkKey(KEY);
//  if( !xbeeDM.error_AT ) {
//    XBee.println("Key set OK");
//  } 
//  else {
//    success = 0;
//    XBee.println("Error while setting Key");  
//  }

  // Set Node name
  xbeeDM.setNodeIdentifier(NODE);
  if( !xbeeDM.error_AT ) {
    XBee.println(xbeeDM.getNodeIdentifier());
  } else {
    success = false;
    XBee.println("Error while setting Node name");
  }

  //setting the highest power level (4 = 18dBm for pro, 0 = 10dBm for pro international);
  xbeeDM.setPowerLevel(4);
    if( !xbeeDM.error_AT ) {
    XBee.println("Set Power Level to max");
  } else {
    success = false;
    XBee.println("Error while setting Power Level");
  }
  // Set scan time
  uint8_t scanTime[2] = {0x01, 0x00};
  xbeeDM.setScanningTime(scanTime);

  // Keep values
  xbeeDM.writeValues();
  if( !xbeeDM.error_AT ) {
    XBee.println("Changes stored OK");
  } 
  else {
    success = false;
    XBee.println("Error while storing values");  
  }

  return success;
}


boolean createFileOnSD(char* filename) {
  if(SD.create(filename)) {
    XBee.println("File Created on SD");
    return true;
  }else{
    XBee.println("Error Creating File");
    return false;
  }
}

boolean appendToSD(char* filename, char* data) {
  if(SD.append(filename, data)) {
    XBee.println("Data appended");
    return true;
  }else{
    XBee.println("Error appending data");
    return false;
  }
}


int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

