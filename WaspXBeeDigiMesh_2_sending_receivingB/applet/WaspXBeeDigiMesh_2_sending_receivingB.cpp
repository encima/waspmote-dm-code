/*
 *  ------Waspmote XBee DigiMesh Sending & Receiving Example------
 *
 *  Explanation: This example shows how to send and receive packets
 *  using Waspmote XBee DigiMesh API
 *
 *  This code sends a packet to another node and waits for an answer from
 *  it. When the answer is received it is shown.
 *
 *  This is the code for the receiver.
 *
 *  Copyright (C) 2009 Libelium Comunicaciones Distribuidas S.L.
 *  http://www.libelium.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Version:                0.2
 *  Design:                 David Gasc\u00f3n
 *  Implementation:    Alberto Bielsa
 */
 
 #include "WProgram.h"
void setup();
void loop();
packetXBee* paq_sent;
 int8_t state=0;
 long previous=0;
 char*  data="Test message!";
 uint8_t destination[8];
 uint8_t i=0;
 char* macHigh="          ";
 char* macLow="           ";
 
void setup()
{
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  // Powers XBee
  xbeeDM.ON();
  
  int counter = 0;  
  while(xbeeDM.getOwnMac()==1&&counter<4){
    xbeeDM.getOwnMac();
    counter++;
  }

  Utils.hex2str(xbeeDM.sourceMacHigh,macHigh,4);
  Utils.hex2str(xbeeDM.sourceMacLow,macLow,4);
  sprintf(data,"-mac:%s%s",macHigh,macLow,'%','\r','\n');
  XBee.println(data);
}

void loop()
{
  // Waiting message
  previous=millis();
  while( (millis()-previous) < 20000 )
  {
    if( XBee.available() )
    {
      xbeeDM.treatData();
      if( !xbeeDM.error_RX )
      {
         // Sending answer back
         while(xbeeDM.pos>0)
         {
           if( (xbeeDM.packet_finished[xbeeDM.pos-1]->naO[0]==0x56) && (xbeeDM.packet_finished[xbeeDM.pos-1]->naO[1]==0x78) )
           {
             paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
             paq_sent->mode=UNICAST;
             paq_sent->MY_known=0;
             paq_sent->packetID=0x52;
             paq_sent->opt=0; 
             xbeeDM.hops=0;
             xbeeDM.setOriginParams(paq_sent, "ACK", NI_TYPE);
             while(i<4)
             {
               destination[i]=xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[i];
               i++;
             }
             while(i<8)
             {
               destination[i]=xbeeDM.packet_finished[xbeeDM.pos-1]->macSL[i-4];
               i++;
             }
             xbeeDM.setDestinationParams(paq_sent, destination, data, MAC_TYPE, DATA_ABSOLUTE);
             state=xbeeDM.sendXBee(paq_sent);
             if(state==0)
             {
               XBee.println("ok");
             }
             free(paq_sent);
             paq_sent=NULL;
           }
           free(xbeeDM.packet_finished[xbeeDM.pos-1]);   
           xbeeDM.packet_finished[xbeeDM.pos-1]=NULL;
           xbeeDM.pos--;
        } 
      }
    }
  }

  delay(5000);
}



int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

