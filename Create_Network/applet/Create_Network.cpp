#include "WProgram.h"
void setup();
void loop();
int setNetworkParams();
uint8_t  PANID[2]={
  0x12,0x34};
long previous=0;
char*  KEY="dgfc";
char* NODE = "DGFC-02";
packetXBee* paq_sent;
int8_t state=0;
uint8_t destination[8];
uint8_t i=0;

void setup() {
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  // Powers XBee
  xbeeDM.ON();
  int network = setNetworkParams();
  XBee.print("Network paramaters set: ");
  XBee.println(network);  
}

void loop() {
//  XBee.println("Awaiting Message");
//  previous=millis();
//  
//  while( (millis()-previous) < 20000 )
//  {
//    if( XBee.available() )
//    {
//      xbeeDM.treatData();
//      if( !xbeeDM.error_RX )
//      {
//        XBee.println("Received something, no clue what!");
//        while(xbeeDM.pos>0)
//        {
//          if( (xbeeDM.packet_finished[xbeeDM.pos-1]->naO[0]==0x56) && (xbeeDM.packet_finished[xbeeDM.pos-1]->naO[1]==0x78) )
//          {
//            paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
//            paq_sent->mode=UNICAST;
//            paq_sent->MY_known=0;
//            paq_sent->packetID=0x52;
//            paq_sent->opt=0; 
//            xbeeDM.hops=0;
//            xbeeDM.setOriginParams(paq_sent, "ACK", NI_TYPE);
//            while(i<4)
//            {
//              destination[i]=xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[i];
//              i++;
//            }
//            while(i<8)
//            {
//              destination[i]=xbeeDM.packet_finished[xbeeDM.pos-1]->macSL[i-4];
//              i++;
//            }
//            xbeeDM.setDestinationParams(paq_sent, destination, "ACK", MAC_TYPE, DATA_ABSOLUTE);
//            state=xbeeDM.sendXBee(paq_sent);
//            if(state==0)
//            {
//              XBee.println("ok");
//            }
//            free(paq_sent);
//            paq_sent=NULL;
//          }
//          free(xbeeDM.packet_finished[xbeeDM.pos-1]);   
//          xbeeDM.packet_finished[xbeeDM.pos-1]=NULL;
//          xbeeDM.pos--;
//         }
//      }
//    }
//  }
}

int setNetworkParams() {
  int success = 0;

  // Chosing a channel : channel 0x0D
  xbeeDM.setChannel(0x0D);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Channel set OK");
  } 
  else {
    success = 0;
    XBee.println("Error while changing channel");  
  }

  // Chosing a PANID : PANID=0x1234
  xbeeDM.setPAN(PANID);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("PANID set OK");
  } 
  else {
    success = 0;
    XBee.println("Error while changing PANID");  
  }

  // Enabling security : KEY="WaspmoteKey"
  xbeeDM.encryptionMode(1);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Security enabled");
  } 
  else {
    success = 0;
    XBee.println("Error while enabling security");  
  }

  xbeeDM.setLinkKey(KEY);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Key set OK");
  } 
  else {
    success = 0;
    XBee.println("Error while setting Key");  
  }

  // Set Node name
  xbeeDM.setNodeIdentifier(NODE); // Set name of OceanWasp01
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Name set OK");
  } 
  else {
    success = 0;
    XBee.println("Error while setting Node name");
  }

  // Keep values
  xbeeDM.writeValues();
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Changes stored OK");
  } 
  else {
    success = 0;
    XBee.println("Error while storing values");  
  }

  return success;
}



int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

