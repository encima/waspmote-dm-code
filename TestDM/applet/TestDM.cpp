/*
 *  ------Waspmote XBee DigiMesh Creating a Network Example------
 *
 *  Explanation: This example shows how to create a Network using Waspmote
 *  XBee DigiMesh API
 *
 *  Note: XBee modules must be configured at 38400bps and with API enabled.
 *
 *  Copyright (C) 2009 Libelium Comunicaciones Distribuidas S.L.
 *  http://www.libelium.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Version:                0.2
 *  Design:                 David Gasc\u00f3n
 *  Implementation:    Alberto Bielsa
 */

#include "WProgram.h"
void setup();
void loop();
uint8_t  PANID[2]={
  0x12,0x34};
char*  KEY="WaspmoteKey";
int count = 0;

void setup()
{
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);

  // Powers XBee
  xbeeDM.ON();
}

void loop()
{
  XBee.println(count);
  XBee.println("Starting Scan");
  xbeeDM.scanNetwork();
  if(xbeeDM.totalScannedBrothers > 0) {
    XBee.println("Sensors Found!");
    for(int i = 0; i < xbeeDM.totalScannedBrothers; i++) {
      XBee.println(RTC.getTime());
      XBee.println("Node Identifier:");
      XBee.println(xbeeDM.scannedBrothers[i].NI);
      XBee.println("RSSI: ");
      XBee.println(xbeeDM.scannedBrothers[i].RSSI, HEX);
      XBee.println("---------");
    }
  } 
  else {
    XBee.println("No Sensors Found");
  }
  count++;
  
  Utils.setLED(LED0, LED_ON);
  Utils.setLED(LED1, LED_ON);
  Utils.setLED(LED0, LED_OFF);
  Utils.setLED(LED1, LED_OFF);
  for (int i=0;i<24;i++){
    Utils.blinkLEDs(125);
  }
}








int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

